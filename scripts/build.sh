#!/usr/bin/env sh

set -e

if [ -z "$GROUP" ] ; then
  echo "Cannot find GROUP env var"
  exit 1
fi

if [ -z "$COMMIT" ] ; then
  echo "Cannot find COMMIT env var"
  exit 1
fi

$(docker -v >/dev/null 2>&1)
if [ $? -eq 0 ]; then
    DOCKER_CMD=docker
else
    DOCKER_CMD=`sudo docker`
fi

REPO=${GROUP}/${PROJECT};

$DOCKER_CMD build -t ${REPO}:${COMMIT} .
